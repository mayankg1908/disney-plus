import React from 'react';
import Navbar from './components/Navbar';
import Home from './components/Home';
import LoginPage from './components/LoginPage';
import { Routes, Route } from 'react-router-dom';
import Detail from './components/Detail';
import Search from './components/Search';
import WatchList from './components/WatchList';

const App = () => {
  return (
    <div className='max-h-screen text-white'>
      <div className='bg-gray-900 pb-2'>
        <Navbar />
        <Routes>
          <Route path='/' element={<LoginPage />} />
          <Route path='/home' element={<Home />} />
          <Route path='/search' element={<Search />} />
          <Route path='/watchList' element={<WatchList />} />
          <Route path='/detail/:id' element={<Detail />} />
        </Routes>
      </div>
    </div>
  );
};

export default App;
