import { initializeApp } from 'firebase/app';
import {
  getFirestore,
  collection,
  getDocs,
  getDoc,
  where,
  query,
  doc,
  onSnapshot,
  setDoc,
  updateDoc,
  arrayUnion,
} from 'firebase/firestore';
import {
  GoogleAuthProvider,
  getAuth,
  signInWithPopup,
  onAuthStateChanged,
  signOut,
} from 'firebase/auth';
import { getStorage } from 'firebase/storage';
import { setMovies } from './features/movie/movieSlice';

const firebaseConfig = {
  apiKey: 'AIzaSyCl03xPg__n_sDoJ-T8z7ZYHl28VHeMwLU',
  authDomain: 'show-edf54.firebaseapp.com',
  projectId: 'show-edf54',
  storageBucket: 'show-edf54.appspot.com',
  messagingSenderId: '957761164537',
  appId: '1:957761164537:web:2a1c4fde74fe644dae7e28',
  measurementId: 'G-KG5GNJK8Q2',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const provider = new GoogleAuthProvider();
const db = getFirestore(app);
const storage = getStorage(app);

export const handleAuth = async (setUser, username) => {
  if (!username) {
    try {
      const result = await signInWithPopup(auth, provider);
      setUser(result?.user);
      const email = result?.user.email;
      const userRef = doc(db, 'users', email);
      const userInfo = await getDoc(userRef);
      if (!userInfo._document) {
        await setDoc(userRef, {
          email: email,
          watchList: [],
        });
      }
      console.log(result.user.email, typeof result.user.email);
    } catch (error) {
      console.log(error);
    }
  } else if (username) {
    try {
      await signOut(auth);
      setUser();
    } catch (error) {}
  }
};
const colRef = collection(db, 'movies');
export const getMovies = async () => {
  try {
    const snapshot = await getDocs(colRef);
    let data = [];
    snapshot.docs.map((doc) => {
      data = [...data, { id: doc.id, ...doc.data() }];
    });
    return data;
    // setMovieData(data);
  } catch (error) {
    alert('ERROR:' + error);
  }
};

export const myOnAuthStateChanged = (setUser, navigate) => {
  onAuthStateChanged(auth, async (user) => {
    if (user) {
      setUser(user);
      navigate('/home');
    }
  });
};

export const getMovieOfID = async (id, setMovieOfID) => {
  const docRef = doc(db, 'movies', id);
  console.log(docRef);
  try {
    onSnapshot(docRef, (doc) => {
      setMovieOfID(doc.data());
    });
  } catch (error) {
    console.log('ERROR: ' + error);
  }
};

export const addToWatchList = async (email, id) => {
  try {
    await updateDoc(doc(db, 'users', email), {
      watchList: arrayUnion(id),
    });

    return true;
  } catch (error) {
    console.log('ERROR:' + error);
    return false;
  }
};

export const getWatchList = async (email, setUserWatchList, allMovies) => {
  try {
    const q = query(collection(db, 'users'), where('email', '==', email));
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      // console.log([...doc.data().watchList]);
      const watchListIDS = [...doc.data().watchList];
      const results = allMovies?.filter((mov) =>
        watchListIDS.some((id) => {
          return id === mov.id;
        })
      );
      setUserWatchList(results);
    });
  } catch (error) {
    console.log('ERROR: ' + error);
  }
};
