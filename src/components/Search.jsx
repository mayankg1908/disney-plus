import React, { useState } from 'react';
import { BsSearch } from 'react-icons/bs';
import MovieList from './MovieList';
import { useSelector } from 'react-redux';
import { selectAll } from '../features/movie/movieSlice';

const Search = () => {
  const [input, setInput] = useState('');
  const [filterMovies, setFilterMovies] = useState([]);
  const allMovies = useSelector(selectAll);

  const getSearchMovie = (e) => {
    e.preventDefault();
    setFilterMovies(
      allMovies.filter((item) => item.title.toLocaleLowerCase().includes(input))
    );
  };
  return (
    <div className='h-screen'>
      <form
        action=''
        className='p-2 m-6 flex items-center w-1/2 rounded-lg bg-slate-600'
      >
        <BsSearch size={24} className='ml-2' />
        <input
          className='p-4 bg-slate-600 w-full rounded-lg  outline-none'
          placeholder='Search movies...'
          value={input}
          onChange={(e) => {
            setInput(e.target.value.toLocaleLowerCase());
            if (input === '') {
              setFilterMovies([]);
            }
          }}
          type='text'
        />
        <button type='submit' onClick={getSearchMovie}></button>
      </form>
      <div>
        <MovieList movies={filterMovies} />
      </div>
    </div>
  );
};

export default Search;
