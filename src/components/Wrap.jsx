import React from 'react';

const Wrap = ({ children }) => {
  return (
    <div className=' duration-300 hover:scale-105 relative border-2 border-slate-500 rounded-md mx-2 flex items-center justify-center'>
      {children}
    </div>
  );
};

export default Wrap;
