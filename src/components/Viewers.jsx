import React from 'react';
import VIEWER1 from '../assets/images/viewers-disney.png';
import VIEWER2 from '../assets/images/viewers-marvel.png';
import VIEWER3 from '../assets/images/viewers-national.png';
import VIEWER4 from '../assets/images/viewers-pixar.png';
import VIEWER5 from '../assets/images/viewers-starwars.png';
import VIDEO1 from '../assets/videos/1564674844-disney.mp4';
import VIDEO2 from '../assets/videos/1564676115-marvel.mp4';
import VIDEO3 from '../assets/videos/1564676296-national-geographic.mp4';
import VIDEO4 from '../assets/videos/1564676714-pixar.mp4';
import VIDEO5 from '../assets/videos/1608229455-star-wars.mp4';
import Container from './Container';
import Wrap from './Wrap';

const VIEWERS = [
  {
    image: VIEWER1,
    video: VIDEO1,
  },
  {
    image: VIEWER2,
    video: VIDEO2,
  },
  {
    image: VIEWER3,
    video: VIDEO3,
  },
  {
    image: VIEWER4,
    video: VIDEO4,
  },
  {
    image: VIEWER5,
    video: VIDEO5,
  },
];

const Viewers = () => {
  return (
    <Container>
      {VIEWERS.map(({ image, video }) => {
        return (
          <Wrap>
            <img
              className='peer absolute z-20 top-0 object-cover'
              src={image}
              alt=''
            />
            <video
              className='w-full h-full opacity-0 hover:opacity-100 hover:shadow-lg peer-hover:opacity-100'
              autoPlay={true}
              loop={true}
              playsInline={true}
              muted
            >
              <source src={video} type='video/mp4' />
            </video>
          </Wrap>
        );
      })}
    </Container>
  );
};

export default Viewers;
