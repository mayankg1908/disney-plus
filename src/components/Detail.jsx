import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { addToWatchList, getMovieOfID } from '../firebase';
import { BsPlayFill } from 'react-icons/bs';
import { IoMdAdd } from 'react-icons/io';
import { useSelector } from 'react-redux';
import { selectUserEmail } from '../features/user/userSlice';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Detail = () => {
  const [movieOfID, setMovieOfID] = useState({});
  const [isSuccess, setIsSuccess] = useState('');
  const email = useSelector(selectUserEmail);
  const id = useParams().id;
  useEffect(() => {
    getMovieOfID(id, setMovieOfID);
  }, [id]);
  const { title, image } = movieOfID;
  return (
    <div className='min-h-screen'>
      <ToastContainer autoClose={2000} />
      <div className='hero-content flex-col mx-auto items-center justify-center  lg:flex-row-reverse'>
        <img src={image} className='max-w-sm rounded-lg  shadow-2xl' />
        <div>
          <h1 className='text-5xl font-bold'>{title}</h1>
          <p className='py-6'>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam illum
            unde, dolorem animi delectus vitae ipsam alias aspernatur provident
            dicta?
          </p>
          <button className='btn btn-primary mr-4'>
            <BsPlayFill /> Play
          </button>
          <button className='btn glass'>
            <BsPlayFill /> Trailer
          </button>
          <button
            onClick={() => {
              addToWatchList(email, id, toast)
                ? toast.success('Added to Watch List')
                : toast.error('Some Error Occured');
            }}
            className='btn btn-circle ml-4'
          >
            <IoMdAdd />
          </button>
        </div>
      </div>
    </div>
  );
};

export default Detail;
