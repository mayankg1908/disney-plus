import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import userSlice, { selectUserEmail } from '../features/user/userSlice';
import { getWatchList } from '../firebase';
import MovieList from './MovieList';
import { selectAll } from '../features/movie/movieSlice';

const WatchList = () => {
  const [userWatchList, setUserWatchList] = useState([]);
  const email = useSelector(selectUserEmail);
  const allMovies = useSelector(selectAll);
  const [filteredMovies, setFilteredMovies] = useState(null);

  useEffect(() => {
    (async () => {
      await getWatchList(email, setUserWatchList, allMovies);
    })();
  }, [email]);

  return (
    <div className=''>
      <MovieList movies={userWatchList} />
    </div>
  );
};

export default WatchList;
