import React, { useEffect } from 'react';
import Hero from './Hero';
import Viewers from './Viewers';
import { useDispatch, useSelector } from 'react-redux';
import { getMovies } from '../firebase';
import { selectNewDisney, selectOriginal, selectRecommend, selectTrending, setMovies } from '../features/movie/movieSlice';
import { selectUserName } from '../features/user/userSlice';
import MovieList from './MovieList';

const Home = () => {
  const username = useSelector(selectUserName);
  const dispatch = useDispatch();
  const recommends = useSelector(selectRecommend);
  const newDisneys = useSelector(selectNewDisney);
  const originals = useSelector(selectOriginal);
  const trendings = useSelector(selectTrending);
  useEffect(() => {
    let recommends = [];
    let originals = [];
    let newDisney = [];
    let trending = [];
    const fetchMovies = async () => {
      const data = await getMovies();
      let all = [...data];
      console.log('All:', all);
      data?.map((item) => {
        switch (item.type) {
          case 'recommend':
            recommends = [...recommends, item];
            break;
          case 'original':
            originals = [...originals, item];
            break;
          case 'new':
            newDisney = [...newDisney, item];
            break;
          case 'trending':
            trending = [...trending, item];
            break;

          default:
            break;
        }
      });
      dispatch(
        setMovies({
          all: all,
          recommend: recommends,
          newDisney: newDisney,
          original: originals,
          trending: trending,
        })
      );
    };
    fetchMovies();
  }, [username]);
  return (
    <div className='h-full'>
      <Hero />
      <Viewers />
      <MovieList title="recommends for you" movies={recommends} />
      <MovieList title="Trending" movies={trendings} />
      <MovieList title="New Disney" movies={newDisneys} />
      <MovieList title="originals" movies={originals} />
    </div>
  );
};

export default Home;
