import React from 'react';
import slide1 from '../assets/images/slider-badag.jpg';
import slide2 from '../assets/images/slider-badging.jpg';
import slide3 from '../assets/images/slider-scale.jpg';
import slide4 from '../assets/images/slider-scales.jpg';

const Hero = () => {
  return (
    <div className="">
      <div className='carousel w-full '>
        <div id='item1' className='carousel-item w-full'>
          <img src={slide1} className='w-full' />
        </div>
        <div id='item2' className='carousel-item w-full'>
          <img src={slide2} className='w-full' />
        </div>
        <div id='item3' className='carousel-item w-full'>
          <img src={slide3} className='w-full' />
        </div>
        <div id='item4' className='carousel-item w-full'>
          <img src={slide4} className='w-full' />
        </div>
      </div>
      <div className='flex justify-center w-full py-2 gap-2'>
        <div>
          <a href='#item1'id='item-1' className='btn btn-xs '></a>
        </div>
        <a href='#item2' id='item-2' className='btn btn-xs'></a>
        <a href='#item3' id='item-3' className='btn btn-xs'></a>
        <a href='#item4' id='item-4' className='btn btn-xs'></a>
      </div>
    </div>
  );
};

export default Hero;
