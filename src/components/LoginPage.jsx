import React from 'react';
import ctaLogo1 from '../assets/images/cta-logo-one.svg';
import ctaLogo2 from '../assets/images/cta-logo-two.png';
import { handleAuth } from '../firebase';
import LOGINBG from '../assets/images/login-background.jpg';
// import { selectUserEmail, selectUserPhoto, selectUserName } from '../features/user/userSlice';

const LoginPage = () => {
  return (
    <div
      style={{
        background: `url(${LOGINBG})`,
      }}
      className='text-white bg-black h-screen flex flex-col sm:items-center sm:justify-center'
    >
      <div className='p-4 flex flex-col gap-3 justify-center items-center'>
        <img className='w-5/6' src={ctaLogo1} alt='' />
        <button
          // onClick={() => handleAuth()}
          className='rounded-lg uppercase font-semibold tracking-wider duration-300 hover:bg-blue-800 bg-blue-700 p-5 w-5/6'
        >
          Get All There
        </button>
        <p className='w-5/6'>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam,
          fugiat! Nemo deleniti unde placeat ab!
        </p>
        <img className='w-5/6 mt-4' src={ctaLogo2} alt='' />
      </div>
    </div>
  );
};

export default LoginPage;
