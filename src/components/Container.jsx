import React from 'react';

const Container = ({children}) => {
  return (
    <div className='grid  items-center justify-center gap-4 sm:grid-cols-2 md:grid-cols-5'>
      {children}
    </div>
  );
};

export default Container;
