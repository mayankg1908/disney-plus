import React, { useEffect } from 'react';
import LOGO from '../assets/images/logo.svg';
import HOME from '../assets/images/home-icon.svg';
import WATCHLIST from '../assets/images/watchlist-icon.svg';
import SEARCH from '../assets/images/search-icon.svg';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectUserName,
  selectUserEmail,
  selectUserPhoto,
  setUserLoginDetails,
  setSignOutState,
} from '../features/user/userSlice';
import { Link, useNavigate } from 'react-router-dom';
import { getMovies, handleAuth, myOnAuthStateChanged } from '../firebase';
import { onAuthStateChanged } from 'firebase/auth';

const NAVITEMS = [
  {
    icon: HOME,
    name: 'HOME',
  },
  {
    icon: SEARCH,
    name: 'SEARCH',
  },
  {
    icon: WATCHLIST,
    name: 'WATCHLIST',
  },
];

const Navbar = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const username = useSelector(selectUserName);
  const userPhoto = useSelector(selectUserPhoto);
  const setUser = (user = null) => {
    if (!user) {
      dispatch(setSignOutState())
      navigate('/');
    } else
      dispatch(
        setUserLoginDetails({
          name: user.displayName,
          email: user.email,
          photo: user.photoURL,
        })
      );
  };
  useEffect(() => {
    myOnAuthStateChanged(setUser, navigate);
    getMovies()
  }, [username]);

  return (
    <div className='flex items-center justify-between bg-gray-900 p-6 pb-6 text-white'>
      <Link to='/home'>
        <img className='hidden sm:block w-16' src={LOGO} alt='' />
      </Link>
      <div className='flex justify-center items-center gap-4 ml-auto font-semibold'>
        {!username ? (
          <button
            onClick={() => handleAuth(setUser, username)}
            className='btn hover:bg-black hover: border-2 border-white rounded-lg px-4 py-2 ml-8'
          >
            Login
          </button>
        ) : (
          <>
            {NAVITEMS.map(({ icon, name }) => (
              <div className='flex items-center text-xs tracking-wide'>
                <img className='w-7' src={icon} alt='' />
                <Link to={`/${name}`}>{name}</Link>
              </div>
            ))}
            <div className='dropdown dropdown-bottom dropdown-end dropdown-hover'>
              <div className='avatar'>
                <div className='w-12 rounded-full ring ring-primary ring-offset-base-100 ring-offset-2'>
                  <img src={userPhoto} />
                </div>
                <ul
                  tabIndex={0}
                  className='dropdown-content menu p-2 shadow bg-base-100 rounded-box w-52'
                >
                  <li>
                    <button className='btn btn-primary' onClick={() => handleAuth(setUser, username)}>Sign Out</button>
                  </li>
                </ul>
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default Navbar;
