import React, { useEffect } from 'react';
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';
import { Link } from 'react-router-dom';

const MovieList = ({ movies, title }) => {
  console.log("Mayank :" + movies);
  return (
    <div className='my-16 px-8'>
      <h1 className='text-2xl font-semibold mb-4'>{title}</h1>
      <div className='grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-4'>
        { movies ? (
          movies.map((item) => {
            return (
              <div className='border-yellow-50 h-64 hover:scale-105 duration-300'>
                <Link to={'/detail/' + item.id}>
                  <img
                    className='rounded-lg object-cover h-full w-full'
                    src={item.image}
                    alt=''
                  />
                </Link>
              </div>
            );
          })
        ) : (
          <>
            <Skeleton className='h-64' baseColor='#22334f' />
            <Skeleton className='h-64' baseColor='#22334f' />
            <Skeleton className='h-64' baseColor='#22334f' />
            <Skeleton className='h-64' baseColor='#22334f' />
          </>
        )}
      </div>
    </div>
  );
};

export default MovieList;
