/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      backgroundImage: {
        'hero-pattern': "url('./src/assets/images/home-background.png')",
        'login-page': "url('./src/assets/images/login-background.jpg')",
      }
    },
  },
  plugins: [require("daisyui")],
};
